#!/usr/bin/env python

from __future__ import print_function
import sys
import os
import re

def trim_aa(s):
    s = re.sub('^[^C]*Y[LFI](C)', '\\1', s)
    s = re.sub('([FW])(G.G|G.|G)$', '\\1', s)
    return s
    
AAcode={"TTT":"F","TTC":"F","TTA":"L","TTG":"L",
        "TCT":"S","TCC":"S","TCA":"S","TCG":"S",
        "TAT":"Y","TAC":"Y","TAA":"*","TAG":"*",
        "TGT":"C","TGC":"C","TGA":"*","TGG":"W",
        "CTT":"L","CTC":"L","CTA":"L","CTG":"L",
        "CCT":"P","CCC":"P","CCA":"P","CCG":"P",
        "CAT":"H","CAC":"H","CAA":"Q","CAG":"Q",
        "CGT":"R","CGC":"R","CGA":"R","CGG":"R",
        "ATT":"I","ATC":"I","ATA":"I","ATG":"M",
        "ACT":"T","ACC":"T","ACA":"T","ACG":"T",
        "AAT":"N","AAC":"N","AAA":"K","AAG":"K",
        "AGT":"S","AGC":"S","AGA":"R","AGG":"R",
        "GTT":"V","GTC":"V","GTA":"V","GTG":"V",
        "GCT":"A","GCC":"A","GCA":"A","GCG":"A",
        "GAT":"D","GAC":"D","GAA":"E","GAG":"E",
        "GGT":"G","GGC":"G","GGA":"G","GGG":"G"}

def fix_frame(dna, aa, n=3):
    ''' Make sure that the DNA sequence directly translates the AA sequence'''
    for i in range(len(dna)):
        new = dna[i:]
        if len(new) < n*3:
            break 
        trans = ''
        for j in range(n):
            trans += AAcode[new[j*3 : j*3+3]]
        if trans == aa[:n]:
            return new
    return None ## fail to fix

def format_aa_dna(aa, dna, contig):
    new_aa = trim_aa(aa)
    if dna not in contig:
        contig = contig.replace('A','t').replace('T','a')
        contig = contig.replace('C','g').replace('G','c')
        contig = contig.upper()
        contig = contig[::-1]
    new_dna = fix_frame(contig[contig.find(dna):], aa)
    if new_dna is None:
        return '', ''
    idx = aa.find(new_aa)
    new_dna = new_dna[idx*3 : (idx+len(new_aa))*3]
    assert len(new_dna) == len(new_aa)*3
    return new_aa, new_dna

def format_trust(infile, outfile=None):
    print('FROMAT', infile)
    if outfile is None:
        outfile = infile+'.txt'
    out = open(outfile, 'w')
    with open(infile, 'r') as f:
        content = f.readlines()
        prev = None
        for line in content:
            if line.startswith("#"):
                out.write(line)
                continue
            if prev and prev.startswith(">"):
                split = prev[1:-1].split("+")
                aa_seq = split[7]
                dna_seq = split[9]
                contig = line.strip()
                new_aa, new_dna = format_aa_dna(aa_seq, dna_seq, contig)
                if 6 <= len(new_aa) and len(new_aa) <= 30:
                    split[7] = new_aa
                    split[9] = new_dna
                    out.write('>%s\n%s\n'%('+'.join(split), contig))
            prev = line
    return outfile

def format_all(path):
    for f in os.listdir(path):
        if f.endswith('.fa'):
            p = os.path.join(path, f)
            format_trust(p, p+'.txt')

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('Please give the file name or path of TRUST outputs.')
        exit(0)
    if os.path.isdir(sys.argv[1]):
        format_all(sys.argv[1])
    elif os.path.exists(sys.argv[1]):
        format_trust(sys.argv[1])
    else:
        print(sys.argv[1], 'does not exist.')

