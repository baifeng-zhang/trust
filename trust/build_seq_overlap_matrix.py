'''
Build pair-wise sequence sharing matrix, allow multiprocessing.
'''


from multiprocessing import Pool
from functools import partial
from trust.process_seq import *
from trust.find_overlap_seq import *
import random

'''Modify this number to use multiprocessing
'''
NUM_OF_THREADS = 1

'''
Max read number in each gene loci without linearly split
'''
MAX_READS_NUMBER = 1500
MAX_READS_THRE = 0.15



def OrderUnmappedReads(geneObj):
    '''
    Preliminary ordering of unmapped reads using their mapped pair
    '''
    urList=[]

    for vv in geneObj:
        if vv[0]==1:
            urList.append((vv[1][1].pos,vv[1][0]))
        if vv[0]==2:
            urList.append((vv[1][0].pos,vv[1][1]))

    urList_sorted=sorted(urList,key=lambda x:x[0])

    return urList_sorted


def SplitUnmappedReads(urList_sorted, ns):

    pos_min=urList_sorted[0][0]
    pos_max=urList_sorted[-1][0]

    group_length=(pos_max-pos_min)/ns

    UR_sliced=[]
    tmpList=[]
    cur_pos=pos_min

    for kk in urList_sorted:
        if kk[0] <= cur_pos+group_length:
            tmpList.append(kk[1])
        else:
            UR_sliced.append(tmpList)
            cur_pos=kk[0]   
            tmpList=[kk[1]]

    UR_sliced_new=[]    
    for i in UR_sliced:
        if len(i) > 500:   
            UR_sliced_new.append(random.sample(i, 500))
        else:
            UR_sliced_new.append(i)
    #print len(UR_sliced)
    return UR_sliced

def fun_map(p,f):
    ## Fake function for passing multiple arguments to Pool.map()
    return f(*p)

def GetSlicedReadsOverlap(reads, BitReads, qNames, nr=50, err=1, overlap_thr=10):

    OverlapInfo={}
    temp=[]
    Nu=len(BitReads)

    for i in xrange(0,Nu):
        x=BitReads[i]
        xRead=reads[i]
        for j in xrange(i,Nu):
            if j==i:
                continue
            
            y=BitReads[j]
            yRead=reads[j]
            sL=PairwiseReadComparison(xRead, yRead, x,y,n=nr,err=err)
            tmpMax=-1
            tmpIdx=-1
            
            for k in xrange(0,4):
                if sL[k][0]>tmpMax:
                    tmpMax=sL[k][0]
                    tmpIdx=k
            
            if tmpMax>=overlap_thr:
                qnn=qNames[i]+'\t'+qNames[j]
                if qNames[i] not in temp:
                    temp.append(qNames[i])
                if qNames[j] not in temp:
                    temp.append(qNames[j])
                OverlapInfo[qnn]=(sL[tmpIdx],tmpIdx)

    return temp, OverlapInfo


def GetReadsOverlapByGene(geneObj,nr=50,err=1,overlap_thr=10):

    '''
    Bulid pair-end reads overlap matrix
    '''

    UnmappedReads=[]
    temp=[]
    UR_sliced=[]
    
    urLocationSorted=OrderUnmappedReads(geneObj)

    for i in urLocationSorted:
        UnmappedReads.append(i[1])
    nc=len(UnmappedReads)

    if len(UnmappedReads) > MAX_READS_NUMBER:

        '''  
        ns=len(UnmappedReads)/MAX_READS_NUMBER+1
        print "-----dividing unmapped reads into %d slices" %(ns)
        
        UR_sliced=SplitUnmappedReads(urLocationSorted, ns)
        
        '''
        ns=nc/MAX_READS_NUMBER
        print "-----dividing unmapped reads into %d slices" %(ns)
        slice_length = MAX_READS_NUMBER #nc/ns
        st=0
        while st<nc:
            ed=st+slice_length
            if nc-ed < MAX_READS_THRE*slice_length or ed>nc:
                ed=nc
            UR_sliced.append(UnmappedReads[st:ed])
            st=ed
        #'''
        
    else:

        UR_sliced=[UnmappedReads]

    if(len(UR_sliced) > 1 and NUM_OF_THREADS > 1):  # multiple processing
        
        para = []

        for UR in UR_sliced:
            reads=[]
            BitReads=[]
            qNames=[]
            for read in UR:
                # print len(read.seq)
                if len(read.seq) != nr:
                    continue
                reads.append(read.seq)
                qNames.append(read.qname)
                BitReads.append(ConvertDNAtoBinary(read.seq))
            para.append((reads, BitReads, qNames, nr, err, overlap_thr))

        ## start multiple processing
        pl = Pool(NUM_OF_THREADS)
        pool_out = pl.map(partial(fun_map, f=GetSlicedReadsOverlap), para)
        pl.close()
        pl.join()
        ## end multiple processing
        
        temp =[]
        OverlapInfo={}
        for i,j in pool_out:
            temp.extend(i)
            OverlapInfo = dict(OverlapInfo, **j)
    else:

        OverlapInfo={}
        for UR in UR_sliced:

            reads=[]
            BitReads=[]
            qNames=[]
            for read in UR:
                if len(read.seq) != nr:
                    continue
                reads.append(read.seq)
                qNames.append(read.qname)
                BitReads.append(ConvertDNAtoBinary(read.seq))
            Nu=len(BitReads)
            for i in xrange(0,Nu):
                x=BitReads[i]
                xRead=reads[i]
                for j in xrange(i,Nu):
                    if j==i:
                        continue
                    y=BitReads[j]
                    yRead=reads[j]
                    sL=PairwiseReadComparison(xRead, yRead, x,y,n=nr,err=err)
                    tmpMax=-1
                    tmpIdx=-1
                    for k in xrange(0,4):
                        if sL[k][0]>tmpMax:
                            tmpMax=sL[k][0]
                            tmpIdx=k
                    if tmpMax>=overlap_thr:
                        #print sL[tmpIdx], x, ConvertBitToDNA(x), y, ConvertBitToDNA(y)
                        qnn=qNames[i]+'\t'+qNames[j]
                        if qNames[i] not in temp:
                            temp.append(qNames[i])
                        if qNames[j] not in temp:
                            temp.append(qNames[j])
                        OverlapInfo[qnn]=(sL[tmpIdx],tmpIdx)
    
    for rr in UnmappedReads:
        if rr.qname not in temp:
            OverlapInfo[rr.qname+'\t'+rr.qname]=((nr,0),0)
    
    #print "OverlapInfo: ", len(OverlapInfo)
    O1 = {}
    for key in sorted(OverlapInfo):
        O1[key] = OverlapInfo[key]

    return O1,UnmappedReads


def GetReadsOverlapByGene_SE(geneObj,nr=75,err=1,overlap_thr=10):
    '''
    Bulid single-end reads overlap matrix
    '''
    UnmappedReads=[]
    temp=[]
    for rr in geneObj:
        UnmappedReads.append(geneObj[rr][0][0])
    
    OverlapInfo={}
    UR_sliced=[]
    nc=len(UnmappedReads)
    
    if nc > MAX_READS_NUMBER:
        
        ns=nc/MAX_READS_NUMBER
        print "-----dividing unmapped reads into %d slices" %(ns)
        slice_length = MAX_READS_NUMBER #nc/ns
        st=0
        while st<nc:
            ed=st+slice_length
            if nc-ed < MAX_READS_THRE*slice_length or ed>nc:
                ed=nc
            UR_sliced.append(UnmappedReads[st:ed])
            st=ed
    else:
        UR_sliced=[UnmappedReads]

    nu=len(UR_sliced)
    
    if(nu > 1 and NUM_OF_THREADS > 1):  #multiple processing
        
        para = []
        
        for kk in xrange(0,nu):

            UR=UR_sliced[kk]
            BitReads=[]
            reads=[]
            qNames=[]
            for read in UR:
                if len(read.seq) != nr:
                    continue
                qNames.append(read.qname)
                reads.append(read.seq)
                BitReads.append(ConvertDNAtoBinary(read.seq))
            para.append((reads, BitReads, qNames, nr, err, overlap_thr))

        ## Start multiple processing
        pl = Pool(NUM_OF_THREADS)
        pool_out = pl.map(partial(fun_map, f=GetSlicedReadsOverlap), para)
        pl.close()
        pl.join()
        ##end multiple processing
        
        temp =[]
        for i,j in pool_out:
            temp.extend(i)
            OverlapInfo = dict(OverlapInfo, **j)
    else:

        for kk in xrange(0,nu):

            UR=UR_sliced[kk]
            BitReads=[]
            reads=[]
            qNames=[]
            
            for read in UR:
                if len(read.seq) != nr:
                    continue
                qNames.append(read.qname)
                reads.append(read.seq)
                BitReads.append(ConvertDNAtoBinary(read.seq))
            Nu=len(BitReads)
            
            for i in xrange(0,Nu):
                x=BitReads[i]
                xRead=reads[i]
                for j in xrange(i,Nu):
                    if j==i:
                        continue
                    y=BitReads[j]
                    yRead=reads[j]
                    sL=PairwiseReadComparison(xRead, yRead, x,y,n=nr,err=err)
                    tmpMax=-1
                    tmpIdx=-1
                    
                    for k in xrange(0,4):
                        if sL[k][0]>tmpMax:
                            tmpMax=sL[k][0]
                            tmpIdx=k
                    
                    if tmpMax>=overlap_thr:
                        qnn=qNames[i]+'\t'+qNames[j]
                        if qNames[i] not in temp:
                            temp.append(qNames[i])
                        if qNames[j] not in temp:
                            temp.append(qNames[j])
                        OverlapInfo[qnn]=(sL[tmpIdx],tmpIdx)
    
    for rr in UnmappedReads:
        if rr.qname not in temp:
            OverlapInfo[rr.qname+'\t'+rr.qname]=((nr,0),0)
    
    #print "SE_OverlapInfo: ", len(OverlapInfo)
    O1 = {}
    for key in sorted(OverlapInfo):
        O1[key] = OverlapInfo[key]

    return O1


def GetSeqOverlap(ContigObj,ContigNames,overlap_thr=10,err=1):
    '''
    Build contigs of assembled sequences overlap matrix
    '''
    
    OverlapInfo={}
    nc=len(ContigObj)
    temp=[]

    if nc > MAX_READS_NUMBER:
        
        ns=nc/MAX_READS_NUMBER+1
        print "-----dividing contigs into %d slices" %(ns)
        slice_length = nc/ns
        CO_sliced=[]
        CN_sliced=[]
        st=0
        while st<nc:
            ed=st+slice_length
            if ed>nc:
                ed=nc
            CO_sliced.append(ContigObj[st:ed])
            CN_sliced.append(ContigNames[st:ed])
            st=ed
    else:

        CO_sliced=[ContigObj]
        CN_sliced=[ContigNames]
    
    nCO=len(CO_sliced)
    
    for kk in xrange(0,nCO):
        
        CO=CO_sliced[kk]
        CN=CN_sliced[kk]
        nk=len(CO)
        
        for i in xrange(0,nk):
            x=CO[i]
            for j in xrange(i,nk):
                if j==i:
                    continue
                
                ## if both contigs belong to V/D/J/C category but different genes, overlap does not result in merging
                gg1=CN[i].split('|')
                gg2=CN[j].split('|')
                
                if gg1[0][0:3] != gg2[0][0:3]:
                    ## different TCR gene, very unlikely to happen
                    continue
                if (gg1[0][0:4] == gg2[0][0:4] and gg1[0] != gg2[0]) or (gg1[0]==gg2[0] and gg1[1]!=gg2[1]):
                    ## same TCR gene, same VDJC category, different genes
                    continue
                
                y=CO[j]
                OP=CompareSuffixSeq(x,y,err=err, count=False)

                if OP[0][0]>=overlap_thr:
                    kk=CN[i]+'\t'+CN[j]
                    if CN[i] not in temp:
                        temp.append(CN[i])
                    if CN[j] not in temp:
                        temp.append(CN[j])
                    OverlapInfo[kk]=OP

        for ii in xrange(0,len(CN)):
            if CN[ii] not in temp:
                OverlapInfo[CN[ii]+'\t'+CN[ii]]=((len(CO[ii]),0),0)
    
 
    return OverlapInfo

